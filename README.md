# README #

This application is built from Spring Rest and Spring Data Jpa.
It sets up 2 restful services in an embedded tomcat server, creating and retrieving data from a h2 in memory database. 

### Follow these steps to run the application: ###

You can run the .jar file (located in "product.Manager\target\product.Manager-0.1.0.jar"); If not exists, generate with maven: "clean package"
Or you can import the project in Eclipse, and run as maven build: "spring-boot:run"

### How to test the services: ###

In the folder "src\main\resources", there is a file named "ProductManager-soapui-project.xml"
This is a soapUI project file, which contains sample calls for each method of the two services: crud operations for Product and Image entities. 
Just run the application and import this file on soapUI. 

### Notes: ###

If port 8080 is already in use, an exception will be thrown. Make sure this port is available at the moment you run the application.
When you first start the application, it will create two categories of products, and then it will add some products to it. 
See the Application.java file to details.
In the folder "src\main\resources", there are too two json files, containing data for creating more Products and Images.
Now you can create, update, read and delete products and images for them.
