package manager.product;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import manager.product.model.Image;
import manager.product.model.Product;

@SpringBootApplication
public class Application {

	private static final Logger log = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) {
		SpringApplication.run(Application.class);
	}

	@Bean
	public CommandLineRunner demo(ProductRepository productRepository, ImageRepository imageRepository) {
		return (args) -> {
			
			productRepository.save(new Product(1L, "Energy Drinks", "Energy Drinks Product Type"));
			productRepository.save(new Product(2L, "Energy Snacks", "Energy Snacks Product Type"));
			
			productRepository.save(new Product(1L, "Monster Energy", "Monster Energy 24 pack"));
			productRepository.save(new Product(1L, "Flying Horse", "Flying Horse 18 pack"));
									
			productRepository.save(new Product(2L, "Jack Links Beef Jerky", "Teriaky 6 pack"));
			productRepository.save(new Product(2L, "Jack Links Bacon Jerky", "Peppered Honey 12 pack"));
			
			imageRepository.save(new Image("Monster PNG", 3L));
			imageRepository.save(new Image("Horse PNG", 4L));
			imageRepository.save(new Image("Teriaky PNG", 5L));
			imageRepository.save(new Image("Bacon PNG", 6L));
												
			log.info("Products found with findAll():");
			log.info("-------------------------------");
			for (Product prd : productRepository.findAll()) {
				log.info(prd.toString());
			}
			log.info("");

			// fetch an individual product by ID
			Product prd = productRepository.findOne(1L);
			log.info("Product found with findOne(1L):");
			log.info("--------------------------------");
			log.info(prd.toString());
			log.info("");
			
			prd = productRepository.findOne(2L);
			log.info("Product found with findOne(2L):");
			log.info("--------------------------------");
			log.info(prd.toString());
			log.info("");

			// fetch customers by name
			log.info("Product found with findByName('Monster'):");
			log.info("--------------------------------------------");
			for (Product monster : productRepository.findByName("Monster")) {
				log.info(monster.toString());
			}
			log.info("");
		};
	}
}