package manager.product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import manager.product.model.Image;

@RestController
public class ImageController {

	@Autowired
	ImageService imageService;
	
	@RequestMapping(method=RequestMethod.GET, value="/getTestImage", produces=MediaType.APPLICATION_JSON_VALUE)
	public Image getTestImage(@RequestParam(value = "name", defaultValue = "test") String name) {
		return new Image("Bacon Image :)", 6L);
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/createImage", consumes=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Image> createImage(@RequestBody Image image){
		
		Image newImage = imageService.create(image);
        return new ResponseEntity<Image>(newImage, HttpStatus.CREATED);
    }

	@RequestMapping(method=RequestMethod.GET, value="/getAllImages", produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Iterable<Image>> getAllImages() {
		
		Iterable<Image> foundImages = imageService.findAll();
		return new ResponseEntity<>(foundImages, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/getImageById/{id}")
	public ResponseEntity<Image> getImageById(@PathVariable Long id) {
						
		Image foundImage = imageService.findOne(id);
		
		if (foundImage == null) 
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
				
		return new ResponseEntity<Image>(foundImage, HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/updateImage", consumes=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Image> updateImage(@RequestBody Image image){
		
		Image updatedImage = imageService.update(image);
        return new ResponseEntity<Image>(updatedImage, HttpStatus.OK);
    }
	
	@RequestMapping(method=RequestMethod.DELETE, value="/dropImageById/{id}")
    public ResponseEntity<Image> excluirCliente(@PathVariable Long id){
        
		Image foundImage = imageService.findOne(id);
       
		if(foundImage==null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        
		imageService.delete(foundImage);
		
        return new ResponseEntity<Image>(HttpStatus.OK);
    }
}