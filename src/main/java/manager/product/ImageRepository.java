package manager.product;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import manager.product.model.Image;

public interface ImageRepository extends CrudRepository<Image, Long> {
	
	List<Image> findByType(String name);
	List<Image> findByProductId(Long productId);
}