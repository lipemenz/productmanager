package manager.product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import manager.product.model.Image;

@Service
public class ImageService {

	@Autowired
	ImageRepository imageRepository;

	public Image create(Image image){
		return imageRepository.save(image);
	}

	public Image update(Image image){
		return imageRepository.save(image);
	}
	
	public Image findOne(Long id) {
		return imageRepository.findOne(id);
	}
	
	public Iterable<Image> findAll(){
		return imageRepository.findAll();
	}

	public void delete(Image image){
		imageRepository.delete(image);
	}
}