package manager.product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import manager.product.model.Product;

@RestController
public class ProductController {

	@Autowired
	ProductService productService;
	
	@RequestMapping(method=RequestMethod.GET, value="/getTestProduct", produces=MediaType.APPLICATION_JSON_VALUE)
	public Product getTestProduct(@RequestParam(value = "name", defaultValue = "test") String name) {
		return new Product(98345L, "Test Product", "This is the description for test product.");
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/createProduct", consumes=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Product> createProduct(@RequestBody Product product){
		
		Product newProduct = productService.create(product);
        return new ResponseEntity<Product>(newProduct, HttpStatus.CREATED);
    }

	@RequestMapping(method=RequestMethod.GET, value="/getAllProducts", produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Iterable<Product>> getAllProducts() {
		
		Iterable<Product> foundProducts = productService.findAll();
		return new ResponseEntity<>(foundProducts, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/getProductById/{id}")
	public ResponseEntity<Product> getProductById(@PathVariable Long id) {
						
		Product foundProduct = productService.findOne(id);
		
		if (foundProduct == null) 
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
				
		return new ResponseEntity<Product>(foundProduct, HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/updateProduct", consumes=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Product> updateProduct(@RequestBody Product product){
		
		Product updatedProduct = productService.update(product);
        return new ResponseEntity<Product>(updatedProduct, HttpStatus.OK);
    }
	
	@RequestMapping(method=RequestMethod.DELETE, value="/dropProductById/{id}")
    public ResponseEntity<Product> excluirCliente(@PathVariable Long id){
        
		Product foundProduct = productService.findOne(id);
       
		if(foundProduct==null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        
		productService.delete(foundProduct);
		
        return new ResponseEntity<Product>(HttpStatus.OK);
    }
}