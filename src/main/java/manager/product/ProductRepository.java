package manager.product;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import manager.product.model.Product;

public interface ProductRepository extends CrudRepository<Product, Long> {
	
	List<Product> findByName(String name);
	List<Product> findByParentId(Long parentId);
}