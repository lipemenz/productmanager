package manager.product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import manager.product.model.Product;

@Service
public class ProductService {

	@Autowired
	ProductRepository productRepository;

	public Product create(Product product){
		return productRepository.save(product);
	}

	public Product update(Product product){
		return productRepository.save(product);
	}
	
	public Product findOne(Long id) {
		return productRepository.findOne(id);
	}
	
	public Iterable<Product> findAll(){
		return productRepository.findAll();
	}

	public void delete(Product product){
		productRepository.delete(product);
	}
}