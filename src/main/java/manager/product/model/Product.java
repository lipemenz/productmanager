package manager.product.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Product {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
    private long parentId;
    private String name;
    private String description;
        
    protected Product() {
    }

	public Product(long parentId, String name, String description) {		
		this.parentId = parentId;
		this.name = name;
		this.description = description;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getParentId() {
		return parentId;
	}

	public void setParentId(long parentId) {
		this.parentId = parentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}    
	
	@Override
    public String toString() {
        return String.format(
                "Product [id=%d, parentId='%s', name='%s', description='%s']",
                id, parentId, name, description);
    }
}